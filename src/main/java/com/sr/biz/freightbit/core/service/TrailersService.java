package com.sr.biz.freightbit.core.service;

/**
 * Created with IntelliJ IDEA.
 * User: johnpel
 */

import java.util.List;

import com.sr.biz.freightbit.core.entity.Trailers;
import com.sr.biz.freightbit.core.exceptions.TrailersAlreadyExistsException;

public interface TrailersService {

    public void addTrailers(Trailers trailers) throws TrailersAlreadyExistsException;

    public void updateTrailers(Trailers trailers);

    public void deleteTrailers(Trailers trailers);

    public Trailers findTrailersById(long trailerId);

    public List<Trailers> findAllTrailers();

    public List<Trailers> findTrailersByTrailerCode(String trailerCode);

}
