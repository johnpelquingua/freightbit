package com.sr.biz.freightbit.core.service;

/**
 * Created by ADMIN on 5/13/14.
 */

import java.util.List;
import com.sr.biz.freightbit.core.entity.Customer;
import com.sr.biz.freightbit.core.exceptions.CustomerAlreadyExistsException;

public interface CustomerService {

       public void addCustomer(Customer customer) throws CustomerAlreadyExistsException;

       public void deleteCustomer(Customer customer);

       public void updateCustomer(Customer customer);

       public List<Customer> findAllCustomer();

       public Customer findCustomerById(long customerId);

       public List<Customer> findCustomerByClientId(Long clientId);

       public Customer findCustomerByName(String customer);

}
