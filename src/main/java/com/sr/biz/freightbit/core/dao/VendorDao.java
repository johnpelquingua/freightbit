package com.sr.biz.freightbit.core.dao;

import com.sr.biz.freightbit.core.entity.Vendor;

import java.util.List;

/**
 * Created by Clarence Victoria on 5/13/14.
 */
public interface VendorDao {

    public void addVendor(Vendor vendor);
    public void deleteVendor(Vendor vendor);
    public List<Vendor> findAllVendor();
    public List<Vendor> findAllVendorByClientId(Long clientId);
    public Vendor findVendorById(Long vendorId);
    public List<Vendor> findVendorByVendorName(String vendorName);
    public void updateVendor(Vendor vendor);
}
