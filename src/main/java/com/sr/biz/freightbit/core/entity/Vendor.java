package com.sr.biz.freightbit.core.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Clarence Victoria on 5/13/14.
 */

@Entity
@Table(name = "vendor", catalog = "freightbit", uniqueConstraints = @UniqueConstraint(columnNames = "vendorId"))
public class Vendor implements Serializable{

    private long vendorId;
    private long clientId;
    private Client client;
    private String vendorCode;
    private String vendorName;
    private String vendorType;
    private String vendorClass;
    private String vendorStatus;
    private String createdTimestamp;
    private String createdBy;
    private String modifiedTimestamp;
    private String modifiedBy;

    public Vendor() {
    }

    public Vendor(long vendorId, long clientId, String vendorCode,
                  String vendorName, String vendorType, String vendorClass,
                  String vendorStatus, String createdTimestamp, String createdBy,
                  String modifiedTimestamp, String modifiedBy) {
        this.vendorId = vendorId;
        this.clientId = clientId;
        this.vendorCode = vendorCode;
        this.vendorName = vendorName;
        this.vendorType = vendorType;
        this.vendorClass = vendorClass;
        this.vendorStatus = vendorStatus;
        this.createdTimestamp = createdTimestamp;
        this.createdBy = createdBy;
        this.modifiedTimestamp = modifiedTimestamp;
        this.modifiedBy = modifiedBy;
    }

    @Id
    @GeneratedValue
    @Column(name = "vendorId", unique = true, nullable = false)
    public long getVendorId() {
        return vendorId;
    }

    public void setVendorId(long vendorId) {
        this.vendorId = vendorId;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CLIENTID", nullable = false)
    public long getClientId() {
        return clientId;
    }

    public void setClientId(long clientId) {
        this.clientId = clientId;
    }

    @Column(name = "vendorCode", nullable = false, length = 30)
    public String getVendorCode() {
        return vendorCode;
    }

    public void setVendorCode(String vendorCode) {
        this.vendorCode = vendorCode;
    }

    @Column(name = "vendorName", nullable = false, length = 50)
    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    @Column(name = "vendorType", nullable = false, length = 10)
    public String getVendorType() {
        return vendorType;
    }

    public void setVendorType(String vendorType) {
        this.vendorType = vendorType;
    }

    @Column(name = "vendorClass", nullable = false, length = 30)
    public String getVendorClass() {
        return vendorClass;
    }

    public void setVendorClass(String vendorClass) {
        this.vendorClass = vendorClass;
    }

    @Column(name = "vendorStatus", nullable = false, length = 10)
    public String getVendorStatus() {
        return vendorStatus;
    }

    public void setVendorStatus(String vendorStatus) {
        this.vendorStatus = vendorStatus;
    }

    @Column(name = "createdTimestamp", nullable = false)
    public String getCreatedTimestamp() {
        return createdTimestamp;
    }

    public void setCreatedTimestamp(String createdTimestamp) {
        this.createdTimestamp = createdTimestamp;
    }

    @Column(name = "createdBy", nullable = false, length = 10)
    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name = "modifiedTimeStamp", nullable = false)
    public String getModifiedTimestamp() {
        return modifiedTimestamp;
    }

    public void setModifiedTimestamp(String modifiedTimestamp) {
        this.modifiedTimestamp = modifiedTimestamp;
    }

    @Column(name = "modifiedBy", nullable = false, length = 10)
    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }
}
