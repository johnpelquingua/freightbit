package com.sr.biz.freightbit.core.entity;

/**
 * Created by ADMIN on 5/13/14.
 */

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;


@Entity
@Table(name = "customers", catalog = "freightbit")

public class Customer implements java.io.Serializable {

    private long customerId;
    private Client client;
    private String customerCode;
    private String customerName;
    private String customerType;
    private String website;
    private String phone;
    private String mobile;
    private String fax;
    private String email;
    private boolean dti;
    private boolean mayorsPermit;
    private boolean aaf;
    private boolean signatureCard;
    private String createdTime;
    private String createdBy;
    private String modifiedTimeStamp;
    private String modifiedBy;


    public Customer(){
    }

    public Customer(long customerId,Client client, String customerName,
                    String customerCode, String customerType,
                    String website, String phone,
                    String mobile, String fax,
                    String email, boolean dti,
                    boolean mayorsPermit, boolean aaf,
                    boolean signatureCard, String createdTime,
                    String createdBy, String modifiedTimeStamp,
                    String modifiedBy){

                    this.customerId = customerId;
                    this.client = client;
                    this.customerName = customerName;
                    this.customerType = customerType;
                    this.customerCode = customerCode;
                    this.website = website;
                    this.phone = phone;
                    this.mobile = mobile;
                    this.fax = fax;
                    this.email = email;
                    this.dti = dti;
                    this.mayorsPermit = mayorsPermit;
                    this.aaf = aaf;
                    this.signatureCard = signatureCard;
                    this.createdTime = createdTime;
                    this.createdBy = createdBy;
                    this.modifiedTimeStamp = modifiedTimeStamp;
                    this.modifiedBy = modifiedBy;
    }


    @Id
    @GeneratedValue
    @Column(name = "customerId", unique = true, nullable = false)
    public long getCustomerId() {
        return customerId;
    }
    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "clientId", nullable = false)
    public Client getClient() {
        return this.client;
    }
    public void setClient(Client client) {
        this.client = client;
    }

    @Column(name = "customerCode" ,unique = true, nullable = false)
    public String getCustomerCode() {
        return customerCode;
    }
    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }

    @Column(name = "customerName",nullable = false)
    public String getCustomerName() {
        return customerName;
    }
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    @Column(name = "customerType", nullable = false)
    public String getCustomerType(String aa) {
        return customerType;
    }
    public void setCustomerType(String customerType) {
        this.customerType = customerType;
    }

    @Column(name = "website", unique = true , nullable = true)
    public String getWebsite() {
        return website;
    }
    public void setWebsite(String website) {
        this.website = website;
    }

    @Column(name = "phone", unique = true, nullable = true)
    public String getPhone() {
        return phone;
    }
    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Column(name ="mobile", unique = true , nullable = true)
    public String getMobile() {
        return mobile;
    }
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    @Column(name ="fax", unique = true , nullable = false)
    public String getFax() {
        return fax;
    }
    public void setFax(String fax) {
        this.fax = fax;
    }

    @Column(name ="email", unique = true , nullable = true)
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    @Column(name ="dti",unique = true,nullable = false)
    public boolean isDti() {
        return dti;
    }
    public void setDti(boolean dti) {
        this.dti = dti;
    }

    @Column(name ="mayorsPermit", unique = true, nullable = false)
    public boolean isMayorsPermit() {
        return mayorsPermit;
    }
    public void setMayorsPermit(boolean mayorsPermit) {
        this.mayorsPermit = mayorsPermit;
    }

    @Column(name ="aaf", unique = true, nullable = false)
    public boolean isAaf() {
        return aaf;
    }
    public void setAaf(boolean aaf) {
        this.aaf = aaf;
    }

    @Column(name ="signatureCard", unique = true,nullable = false)
    public boolean isSignatureCard() {
        return signatureCard;
    }
    public void setSignatureCard(boolean signatureCard) {
        this.signatureCard = signatureCard;
    }

    @Column(name ="createdTime",nullable = false)
    public String getCreatedTime() {
        return createdTime;
    }
    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    @Column(name ="createdBy",nullable = false)
    public String getCreatedBy() {
        return createdBy;
    }
    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    @Column(name ="modifiedTimeStamp", nullable = false)
    public String getModifiedTimeStamp() {
        return modifiedTimeStamp;
    }
    public void setModifiedTimeStamp(String modifiedTimeStamp) {
        this.modifiedTimeStamp = modifiedTimeStamp;
    }

    @Column(name ="modifiedBy", nullable = false)
    public String getModifiedBy() {
        return modifiedBy;
    }
    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

}
