package com.sr.biz.freightbit.core.exceptions;

/**
 * Created by Clarence Victoria on 5/13/14.
 */
public class VendorAlreadyExistsException extends ObjectAlreadyExistsException {

    public VendorAlreadyExistsException(String msg) {
        super(msg);
    }

    public VendorAlreadyExistsException(String msg, Throwable cause) {
        super(msg, cause);
    }

    public VendorAlreadyExistsException(long id) {
        super(id);
    }

    public VendorAlreadyExistsException(long id, Throwable cause) {
        super(id, cause);
    }
}
