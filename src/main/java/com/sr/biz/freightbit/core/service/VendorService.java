package com.sr.biz.freightbit.core.service;

import com.sr.biz.freightbit.core.entity.Vendor;
import com.sr.biz.freightbit.core.exceptions.VendorAlreadyExistsException;

import java.util.List;

/**
 * Created by Clarence Victoria on 5/13/14.
 */
public interface VendorService {

    public void addVendor(Vendor vendor) throws VendorAlreadyExistsException;

    public void updateVendor(Vendor vendor);

    public void deleteVendor(Vendor vendor);

    public List<Vendor> findAllVendorByClientId(Long clientId);

    public List<Vendor> findAllVendor();

    public Vendor findVendorById(Long vendorId);

    public Vendor findVendorByVendorName(String vendorName);

    public void updateLastVisitDate(Vendor vendor);

}
