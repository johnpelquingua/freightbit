package com.sr.biz.freightbit.core.service.impl;

import com.sr.biz.freightbit.core.dao.VendorDao;
import com.sr.biz.freightbit.core.entity.Vendor;
import com.sr.biz.freightbit.core.exceptions.VendorAlreadyExistsException;
import com.sr.biz.freightbit.core.service.VendorService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * Created by Clarence Victoria on 5/13/14.
 */
public class VendorServiceImpl implements VendorService {

    private VendorDao vendorDao;

    public void setVendorDao(VendorDao vendorDao) {
        this.vendorDao = vendorDao;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addVendor(Vendor vendor) throws VendorAlreadyExistsException {
        if (vendorDao.findVendorByVendorName(vendor.getVendorName()) != null){
            throw new VendorAlreadyExistsException(vendor.getVendorName());
        } else {
            vendorDao.addVendor(vendor);
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteVendor(Vendor vendor){
        vendorDao.deleteVendor(vendor);
    }

    @Override
    public List<Vendor> findAllVendorByClientId(Long clientId) {
        return vendorDao.findAllVendorByClientId(clientId);
    }

    @Override
    public List<Vendor> findAllVendor() {
        List<Vendor> result = vendorDao.findAllVendor();
        return result;
    }

    @Override
    public Vendor findVendorById(Long vendorId) {
        return vendorDao.findVendorById(vendorId);
    }

    @Override
    public Vendor findVendorByVendorName(String vendorName) {
        List<Vendor> result = vendorDao.findVendorByVendorName(vendorName);

        if (result != null && !result.isEmpty()){
            return result.get(0);
        }

        return null;
    }

    @Override
    public void updateLastVisitDate(Vendor vendor) {
        vendor.setModifiedTimestamp(new Date());
        vendorDao.updateVendor(vendor);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateVendor(Vendor vendor) {
        vendorDao.updateVendor(vendor);
    }

}
