package com.sr.biz.freightbit.core.dao.impl;

import com.sr.biz.freightbit.core.dao.VendorDao;
import com.sr.biz.freightbit.core.entity.Vendor;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Clarence Victoria on 5/13/14.
 */
@Transactional
public class VendorDaoImpl extends HibernateDaoSupport implements VendorDao {

    private static final Logger log = Logger.getLogger(VendorDaoImpl.class);

    @Override
    public void addVendor(Vendor vendor){
        log.debug("adding a new vendor");
        try{
            Session session = getSessionFactory().getCurrentSession();
            session.save(vendor);
            log.debug("adding vendor successful");
        } catch (Exception e){
            log.error("adding vendor failed", e);
            throw e;
        }
    }

    @Override
    public void deleteVendor(Vendor vendor){
        log.debug("deleting a vendor");
        try{
            Session session = getSessionFactory().getCurrentSession();
            session.delete(vendor);
            log.debug("deleting vendor successful");
        } catch (Exception e) {
            log.error("deleting vendor failed", e);
            throw e;
        }
    }

    @Override
    public List<Vendor> findAllVendor(){
        log.debug("finding all vendor");
        try {
            return getSessionFactory().getCurrentSession().createQuery("from Vendor").list();
        } catch (Exception e) {
            log.error("finding all vendor failed", e);
            throw e;
        }
    }

    @Override
    public List<Vendor> findAllVendorByClientId(Long clientId){
        log.debug("finding vendor instance by client" + clientId);
        try{
            Query query = getSessionFactory().getCurrentSession().createQuery("from Vendor v where v.clientId = :clientId");
            List<Vendor> results = (List<Vendor> ) query.list();
            log.debug("find vendor by client id successful, result size:" + results.size());
            return results;
        } catch (Exception e) {
            log.error("finding vendor by client failed", e);
            throw e;
        }
    }

    @Override
    public Vendor findVendorById(Long vendorId){
        log.debug("finding vendor instance by id" + vendorId);
        try {
            Vendor instance = (Vendor) getSessionFactory().getCurrentSession().get(Vendor.class, vendorId);

            if (instance == null) {
                log.debug("finding successful, no instance found");
            } else {
                log.debug("finding successful, instance found");
            }

            return instance;

        } catch (Exception e) {
            log.error("finding vendor by id failed", e);
            throw e;
        }
    }

    @Override
    public List<Vendor> findVendorByVendorName(String vendorName){
        log.debug("finding vendor instance by vendor name" + vendorName);
        try {
            Query query = getSessionFactory().getCurrentSession().createQuery("from Vendor v where v.vendorName = :vendorName");
            List<Vendor> results = (List<Vendor>) query.list();
            log.debug("find by vendorName successful, result size: " + results.size());
            return results;
        } catch (Exception e) {
            log.error("finding vendor instance by vendor name failed", e);
            throw e;
        }
    }

    @Override
    public void updateVendor(Vendor vendor){
        log.debug(("updating a vendor"));
        try {
            Session session = getSessionFactory().getCurrentSession();
            session.saveOrUpdate(vendor);
            log.debug("update successful");
        } catch (Exception e) {
            log.error("update failed", e);
            throw e;
        }
    }
}
