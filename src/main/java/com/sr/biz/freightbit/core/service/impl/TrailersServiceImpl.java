package com.sr.biz.freightbit.core.service.impl;

/**
 * Created with IntelliJ IDEA.
 * User: johnpel
 */

import java.util.Date;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.sr.biz.freightbit.core.dao.TrailersDao;
import com.sr.biz.freightbit.core.entity.Trailers;
import com.sr.biz.freightbit.core.exceptions.TrailersAlreadyExistsException;
import com.sr.biz.freightbit.core.service.TrailersService;

public class TrailersServiceImpl implements TrailersService {

    private TrailersDao trailersDao;

    public void setTrailersDao(TrailersDao trailersDao){
        this.trailersDao = trailersDao;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addTrailers(Trailers trailers) throws TrailersAlreadyExistsException {
        if (trailersDao.findTrailersByTrailersCode(trailers.getTrailerCode())!=null)
            throw new TrailersAlreadyExistsException(trailers.getTrailerCode());
        else
            trailersDao.addTrailers(trailers);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateTrailers(Trailers trailers) {
        trailersDao.updateTrailers(trailers);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteTrailers(Trailers trailers) {
        trailersDao.deleteTrailers(trailers);
    }

    @Override
      public Trailers findTrailersById(long trailerId) {
        return trailersDao.findTrailersById(trailerId);
    }

    @Override
    public List<Trailers> findAllTrailers() {
        List<Trailers> trailers = trailersDao.findAllTrailers();
        return trailers;
    }

    @Override
    public List<Trailers> findTrailersByTrailerCode(String trailerCode) {
        List<Trailers> result = trailersDao.findTrailersByTrailersCode(trailerCode);
        return result;
    }
}
