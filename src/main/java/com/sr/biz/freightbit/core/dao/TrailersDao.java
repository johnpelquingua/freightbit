package com.sr.biz.freightbit.core.dao;

/**
 * Created with IntelliJ IDEA.
 * User: johnpelquingua.com
 */

import java.util.List;
import com.sr.biz.freightbit.core.entity.Trailers;

public interface TrailersDao {

    public void updateTrailers(Trailers trailers);

    public void addTrailers(Trailers trailers);

    public void deleteTrailers(Trailers trailers);

    public Trailers findTrailersById(long trailerId);

    public List<Trailers> findTrailersByTrailersCode(String trailerCode);

    public List<Trailers> findAllTrailers();

}
