package com.sr.biz.freightbit.core.service.impl;

import com.sr.biz.freightbit.core.dao.CustomerDao;
import com.sr.biz.freightbit.core.entity.Customer;
import com.sr.biz.freightbit.core.exceptions.CustomerAlreadyExistsException;
import com.sr.biz.freightbit.core.service.CustomerService;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;

/**
 * Created by ADMIN on 5/13/14.
 */
public class CustomerServiceImpl implements CustomerService {

    CustomerDao customerDao;

    public CustomerDao getCustomerDao() {
        return customerDao;
    }
    public void setCustomerDao(CustomerDao customerDao) {
        this.customerDao = customerDao;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void addCustomer(Customer customer) throws CustomerAlreadyExistsException{

               if(customerDao.findCustomerByName(customer.getCustomerName()) != null)
                    throw new CustomerAlreadyExistsException(customer.getCustomerName());
               else
                    customerDao.addCustomer(customer);
        }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void deleteCustomer(Customer customer){
        customerDao.deleteCustomer(customer);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public void updateCustomer(Customer customer){
        customerDao.updateCustomer(customer);
    }

    @Override
    public List<Customer> findAllCustomer(){
        List<Customer> customers = customerDao.findAllCustomer();
        return customers;
    }
    
    @Override
    public Customer findCustomerById(long id){
        return customerDao.findCustomerById(id);
    }

    @Override
    public List<Customer> findCustomerByClientId(Long clientId){
        return customerDao.findCustomerByClientId(clientId);
    }

    public Customer findCustomerByName(String customer){
    List<Customer> result = customerDao.findCustomerByName(customer);
        if(result != null && !result.isEmpty())
            return result.get(0);
        return null;
    }

}
