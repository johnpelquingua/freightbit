package com.sr.biz.freightbit.core.dao.impl;

/**
 * Created by ADMIN on 5/13/14.
 */

import static  org.hibernate.criterion.Example.create;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import org.springframework.transaction.annotation.Transactional;

import com.sr.biz.freightbit.core.dao.CustomerDao;
import com.sr.biz.freightbit.core.entity.Customer;

import java.util.List;


@Transactional
public class CustomerDaoImpl extends HibernateDaoSupport implements CustomerDao{

private static final Log Log = LogFactory.getLog(CustomerDao.class);

@Override
public  void addCustomer(Customer customer){
    Log.debug("adding a new customer");
    try{
        Session session = getSessionFactory().getCurrentSession();
        session.save(customer);
    }catch(RuntimeException re){
        Log.error("add failed", re);
        throw re;
    }
}

@Override
public void deleteCustomer(Customer customer){
    Log.debug("deleting a customer...");
    try{
        Session session = getSessionFactory().getCurrentSession();
        session.delete(customer);
        Log.debug("delete successful");
    }catch(RuntimeException re){
        Log.error("delete failed", re);
        throw re;
    }

}

@Override
public void updateCustomer(Customer customer){
    Log.debug("updated customer");
    try{
        Session session = getSessionFactory().getCurrentSession();
        session.update(customer);
        Log.debug("customer updated");
    }catch(RuntimeException re){
        Log.error("updating failed", re);
        throw re;
    }

}

@Override
public List<Customer> findAllCustomer(){
    Log.debug("finding all customer");
    try{
        return getSessionFactory().getCurrentSession().createQuery("from customers").list();

    }catch(RuntimeException re){
        Log.error("failed in finding all customer" ,re);
        throw re;
    }


}

@Override
public Customer findCustomerById(Long id){
    Log.debug("finding customer by id");
    try{
        Customer instance = (Customer) getSessionFactory().getCurrentSession().get(Customer.class, id);
            if(instance == null){
               Log.debug("no id exists");
            }else {Log.debug("id found");}
        return instance;
    }catch(RuntimeException re){
            Log.debug("finding failed", re);
            throw re;
    }
}

@Override
public List<Customer> findCustomerByClientId(Long clientId){
        Log.debug("finding customer by client");
    try{
            Query query =  getSessionFactory().getCurrentSession().createQuery(
                            "from Customer c where c.clientId = :clientId");
            query.setParameter("clientId", clientId);
            List<Customer> results = (List<Customer>) query.list();
            Log.debug("find by client id succesful, result size: "
                   + results.size());
            return results;
    }catch(RuntimeException re){
            Log.error("finding customer failed", re);
            throw re;
    }


}

@Override
public List<Customer> findCustomerByName(String customer){
    Log.debug("getting Customer instance by example");
    try{
        Query query= getSessionFactory().getCurrentSession().createQuery(
                "from c where c.customerName = :customerName" );
        query.setParameter("customerName", customer);
        List<Customer> results = (List<Customer>) query.list();
        Log.debug("find by customer name successful, result size: " + results.size());
        return results;
    }catch(RuntimeException re){
        Log.error("find by customer name failed", re);
        throw re;
        }

    }

}