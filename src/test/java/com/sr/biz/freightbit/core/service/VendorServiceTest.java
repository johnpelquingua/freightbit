package com.sr.biz.freightbit.core.service;

import com.sr.biz.freightbit.core.dao.VendorDao;
import com.sr.biz.freightbit.core.entity.Client;
import com.sr.biz.freightbit.core.entity.Vendor;
import com.sr.biz.freightbit.core.exceptions.UserAlreadyExistsException;
import com.sr.biz.freightbit.core.service.impl.VendorServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Clarence Victoria on 5/16/14.
 */
@ContextConfiguration(locations = {"classpath:/conf/applicationContext-lib.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class VendorServiceTest {

    @Mock
    private VendorDao vendorDao;

    @InjectMocks
    private VendorServiceImpl vendorService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = UserAlreadyExistsException.class)
    public void testAddExistingVendor() {
        Vendor vendor = initVendor();

        Mockito.when(vendorDao.findVendorByVendorName("SRI")).thenReturn(new ArrayList<Vendor>());
        vendorService.addVendor(vendor);
    }

    @Test
    public void testFindAllVendors() {
        long clientId = 1L;
        List<Vendor> vendors = new ArrayList<>();
        vendors.add(initVendor());

        Mockito.when(vendorDao.findAllVendorByClientId(clientId)).thenReturn(vendors);
        List<Vendor> results = vendorService.findAllVendorByClientId(clientId);
        Assert.assertEquals(1, results.size());
    }

    @Test
    public void testFindAllVendorsByClientId() {
        long clientId = 1L;

        List<Vendor> vendors = new ArrayList<>();
        vendors.add(initVendor());
        Mockito.when(vendorDao.findAllVendor()).thenReturn(vendors);

        List<Vendor> results = vendorService.findAllVendorByClientId(clientId);
        Assert.assertEquals(vendors.size(), 1);
    }

    private Vendor initVendor() {
        Client client = new Client("Ernest", new Date(), "Juno", new Date(), "Juno");
        Vendor vendor = new Vendor();
        vendor.setClient(client);
        vendor.setVendorId(13L);
        vendor.setVendorCode("SRI");
        vendor.setVendorName("Solutions Resource Inc.");
        vendor.setVendorType("Trucking");
        vendor.setVendorClass("Economy");
        vendor.setVendorStatus("Active");
        vendor.setCreatedTimestamp("10:00 AM");
        vendor.setCreatedBy("admin");
        vendor.setModifiedTimestamp("11:00 AM");
        vendor.setModifiedBy("admin");

        return vendor;
    }
}
