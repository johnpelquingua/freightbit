package com.sr.biz.freightbit.core.service;

/**
 * Created with IntelliJ IDEA.
 * User: johnpel
 */

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.MockitoAnnotations.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sr.biz.freightbit.core.dao.TrailersDao;

import com.sr.biz.freightbit.core.entity.Vendor;
import com.sr.biz.freightbit.core.entity.Client;
import com.sr.biz.freightbit.core.entity.Trucks;
import com.sr.biz.freightbit.core.entity.Trailers;

import com.sr.biz.freightbit.core.service.impl.TrailersServiceImpl;
import com.sr.biz.freightbit.core.exceptions.TrailersAlreadyExistsException;

@ContextConfiguration(locations = {"classpath:/conf/applicationContext-lib.xml"})
@RunWith(SpringJUnit4ClassRunner.class)

public class TrailersServiceTest {

    @Mock
    private TrailersDao trailersDao;

    @InjectMocks
    private TrailersServiceImpl trailersService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected=TrailersAlreadyExistsException.class)
    public void testAddExistingTrailers() {

        Trailers trailers= initTrailers();

        Mockito.when(trailersDao.findTrailersByTrailersCode("TRAILER")).thenReturn(new ArrayList<Trucks>());
        trailersService.addTrailers(trailers);

    }

    // TODO VENDOR DOMAIN
    @Test
    public void testFindAllTrailersByVendorId() {

    }

    // TODO VENDOR DOMAIN
    @Test
    public void testFindAllTrailers(){
        long trailerId = 1L;
        List <Trailers> trailers = new ArrayList();
        trailers.add(initTrailers());

        Mockito.when(trailersDao.findTrailersByTrailersCode(trailerCode)).thenReturn(trailers);
        List<Trailers> results = trailersService.findTrailersById(trailerId);
        Assert.assertEquals(1, results.size());
    }

    private Trailers initTrailers(){

        Trailers trailers = new Trailers();

        trailers.setTrailerCode("TRAILERTEST");
        trailers.setTrailerType("TOYOTA");
        trailers.setTrailerLength('100');
        trailers.setGrossWeight('10.32');
        trailers.setModelName("KIA");
        trailers.setModelYear("123");
        trailers.setAxle('10');
        trailers.setVin("VIN");
        trailers.setPlateNumber("2014");
        //trailers.setCreatedTimestamp();
        trailers.setCreatedBy("JOHN");
        //trailers.setModifiedTimestamp();
        trailers.setModifiedBy("PEL");

        return trailers;
    }
}
