package com.sr.biz.freightbit.core.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.MockitoAnnotations.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.sr.biz.freightbit.core.dao.TrucksDao;

import com.sr.biz.freightbit.core.entity.Vendor;
import com.sr.biz.freightbit.core.entity.Client;
import com.sr.biz.freightbit.core.entity.Trucks;

import com.sr.biz.freightbit.core.service.impl.TrucksServiceImpl;
import com.sr.biz.freightbit.core.exceptions.TrucksAlreadyExistsException;

@ContextConfiguration(locations = {"classpath:/conf/applicationContext-lib.xml"})
@RunWith(SpringJUnit4ClassRunner.class)
public class TrucksServiceTest {

    @Mock
    private TrucksDao trucksDao;

    @InjectMocks
    private TrucksServiceImpl trucksService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected=TrucksAlreadyExistsException.class)
    public void testAddExistingTrucks() {

        Trucks trucks = initTrucks();

        Mockito.when(trucksDao.findTrucksByTruckCode("TRUCK")).thenReturn(new ArrayList<Trucks>());
        trucksService.addTrucks(trucks);

    }

    // TODO VENDOR DOMAIN
    @Test
    public void testFindAllTrucksByVendorId() {

    }

    // TODO VENDOR DOMAIN
    @Test
    public void testFindAllTrucks(){
        long vendorId = 1L;
        List <Trucks> trucks = new ArrayList();
        trucks.add(initTrucks());

        Mockito.when(trucksDao.findTrucksByTruckCode(truckCode)).thenReturn(trucks);
        List<Trucks> results = trucksService.findTrucksById(truckId);
        Assert.assertEquals(1, results.size());
    }

    private Trucks initTrucks(){

        Trucks trucks = new Trucks();

        trucks.setTruckCode("TEST");
        trucks.setTruckType("CARGO");
        trucks.setPlateNumber("ERN");
        trucks.setModelYear('2014');
        trucks.setEngineNumber('201412');
        trucks.setGrossWeight('10000');
        trucks.setCreatedBy("JOHN");
        trucks.setModifiedBy("PEL");

        return trucks;
    }
}
